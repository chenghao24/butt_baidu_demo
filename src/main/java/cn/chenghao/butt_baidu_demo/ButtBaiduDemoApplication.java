package cn.chenghao.butt_baidu_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ButtBaiduDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(ButtBaiduDemoApplication.class, args);
    }

}
