package cn.chenghao.butt_baidu_demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @ClassName JumpController
 * @Author chenghao
 * @Date 2020/3/12 12:50
 **/
@Controller
public class JumpController {

    /**
     * 跳转index页面
     *
     * @return
     */
    @RequestMapping("/")
    public String jump() {
        return "index";
    }
}
